package com.pinyougou.mapper;


import com.pinyougou.pojogroup.QueryVo;
import com.pinyougou.pojogroup.SellData;

import java.util.List;

public interface SaleMapper {

    List<SellData> findByQueryVo(QueryVo queryVo);
}
