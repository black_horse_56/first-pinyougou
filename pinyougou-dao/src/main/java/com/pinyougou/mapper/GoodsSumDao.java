package com.pinyougou.mapper;

import entity.GoodsSum;


import java.util.ArrayList;
import java.util.Date;

public interface GoodsSumDao {


    ArrayList<GoodsSum> findAllGoods();


    ArrayList<GoodsSum> findByTime(Date date);
}
