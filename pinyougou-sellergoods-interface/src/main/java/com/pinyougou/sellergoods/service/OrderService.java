package com.pinyougou.sellergoods.service;

/**
 * @author 86158
 * @date 2019/4/29
 * @description 这是订单管理接口
 */

import java.util.List;
import java.util.Map;

import com.pinyougou.pojo.TbOrder;

import com.pinyougou.pojogroup.QueryVo;
import entity.PageResult;

/**
 * 服务层接口
 * @author Administrator
 *
 */
public interface OrderService {

    /**
     * 返回全部列表
     * @return list
     */
     List<TbOrder> findAll();

    /**
     * 根据订单号查询订单状态
     * @param orderId 订单号
     * @return 订单列表
     */
     public List<TbOrder> searchById(Long orderId);


    /**
     * 返回分页列表
     * @return page
     */
     PageResult findPage(int pageNum, int pageSize);


    /**
     * 增加
     */
     void add(TbOrder order);


    /**
     * 修改
     */
     void update(TbOrder order);


    /**
     * 根据ID获取实体
     * @param id id
     * @return 一条数据
     */
     TbOrder findOne(Long id);


    /**
     * 分页
     * @param pageNum 当前页码
     * @param pageSize 每页记录数
     * @return 分页
     */
     PageResult findPage(TbOrder order, int pageNum, int pageSize);

    /**
     * 此方法是查询数据库销售额结果
     * @param queryVo 查询
     * @return map
     */
    Map<String, Object> findSellData(QueryVo queryVo);

}
