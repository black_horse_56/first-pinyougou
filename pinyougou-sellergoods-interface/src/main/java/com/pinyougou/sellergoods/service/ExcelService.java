package com.pinyougou.sellergoods.service;

import com.pinyougou.pojo.TbItemCat;
import com.pinyougou.pojo.TbTypeTemplate;

import java.util.List;

public interface ExcelService {

    void importDb(List <List <Object>> list, String type);

    void importDb2(List <TbTypeTemplate> typeTemplates);

    void importDb3(List <TbItemCat> itemCats);

    void importExcel() throws Exception;

    boolean importExcel2();
}
