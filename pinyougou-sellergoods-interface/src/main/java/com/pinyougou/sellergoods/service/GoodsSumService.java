package com.pinyougou.sellergoods.service;

import entity.GoodsSum;
import entity.PageResult;

import java.util.List;

public interface GoodsSumService {

    PageResult findPage(long time, int page, int rows);

    List<GoodsSum> findAll(long time);
}
