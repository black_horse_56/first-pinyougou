package com.pinyougou.page.service;

public interface ItemPageService {

    /**此方法用于生成静态页面
     * @param id
     */
    void genHtml(Long id);

    void deleteHtml(Long id);
}
