package com.pinyougou.cart.security;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.mapper.TbUserMapper;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.pojo.TbUserExample;
import com.pinyougou.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.cart.security *
 * @since 1.0
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    @Reference
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //自定义认证类  作用就是用于授权 ，认证交给CAS服务端
        /**
         *  判断用户是否已经冻结 或 超过3个月未登录
         */

        // 走到这说明账户正常，允许登录，并存到redis 获取在线用户人数
        boolean b = userService.updateUserStatus(username);

        if (b) {

            return new User(username, "", AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
        } else {

            return new User(username, "", AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_NO"));
        }
    }
}
