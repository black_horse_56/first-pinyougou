//控制层
app.controller('goodsSumPieController', function ($scope, $controller, goodsSumPieService) {

    $controller('baseController', {$scope: $scope});//继承

    $scope.time = 1;//定义搜索时间；

    $scope.findAll1=function(){
        goodsSumPieService.findAll($scope.time).success(
            function (response) {
                $scope.list=response;
                myChart.setOption({
                    series: [{
                        // 根据名字对应到相应的系列
                        name: '销量',
                        data: $scope.list
                    }]
                });
            }
        )
    };


    $scope.searchBytime =function (num) {
        if (num==0) {
            $scope.time = 1;
            $scope.findAll1();
        }

        if (num==1){
            var  daytime=1000*60*60*24;
            $scope.time= new Date().getTime()-daytime;
            $scope.findAll1();


        }
        if (num==2){
          var  daytime=1000*60*60*24*7;
            $scope.time= new Date().getTime()-daytime;
            $scope.findAll1();

        }
        if (num==3){
           var   daytime=1000*60*60*24*30;
            $scope.time= new Date().getTime()-daytime;
            $scope.findAll1();

        }
    }
    
    
    
});
