//控制层
app.controller('goodsSumController', function ($scope, $controller, goodsSumService) {

    $controller('baseController', {$scope: $scope});//继承

    $scope.time = 1;//定义搜索时间；

    //搜索
    $scope.search = function (page, rows) {
        goodsSumService.search(page, rows, $scope.time).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    };



    $scope.searchBytime =function (num) {
        if (num==0) {
            $scope.time = 1;
            $scope.reloadList();
        }

        if (num==1){
            var  daytime=1000*60*60*24;
            $scope.time= new Date().getTime()-daytime;
            $scope.reloadList();


        }
        if (num==2){
          var  daytime=1000*60*60*24*7;
            $scope.time= new Date().getTime()-daytime;
            $scope.reloadList();
        }
        if (num==3){
           var   daytime=1000*60*60*24*30;
            $scope.time= new Date().getTime()-daytime;
            $scope.reloadList();
        }
    }
    
    
    
});
