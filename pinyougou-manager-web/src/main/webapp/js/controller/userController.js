app.controller('userController', function ($scope, $controller, userService) {

    $controller('baseController', {$scope: $scope});//继承

    /**
     * 获取用户列表，带分页、搜索功能
     *  冻结 根据 Tbuser 的 status 字段判断 （Y正常 N 冻结）
     *   冻结功能，未做
     */
    $scope.searchEntity = {};//定义搜索对象
    //搜索
    $scope.search = function (page, rows) {
        userService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        )
    };

    /**
     *  获取用户的个数、活跃用户的个数、非活跃用户的个数
     */
    $scope.getUserCountFromStatus = function () {
        userService.getUserCountFromStatus().success(
            function (response) {
                $scope.data = response;
            }
        )
    };

    /**
     *  修改用户状态，手动冻结用户
     */
    $scope.frozenUser = function (userId) {
        userService.frozenUser(userId).success(
            function (response) {
                if (response.success){
                    $scope.reloadList();//重新加载
                } else {
                    alert(response.message)
                }
            }
        )
    }

});