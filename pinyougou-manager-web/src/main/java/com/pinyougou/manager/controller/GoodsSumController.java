package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbItemCat;
import com.pinyougou.sellergoods.service.GoodsSumService;
import entity.GoodsSum;
import entity.PageResult;
import org.apache.ibatis.annotations.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goodsum")
public class GoodsSumController {

    @Reference
    private GoodsSumService goodsSumService;

    @RequestMapping("/search")
    public PageResult search(@RequestBody long time, int page, int rows  ){
        PageResult page1 = goodsSumService.findPage(time, page, rows);
        return page1;
    }

    @RequestMapping("/findAll")
    public List<Map> findAll(@RequestBody long time){
        List<GoodsSum> list = goodsSumService.findAll(time);
        List<Map> list1 =new ArrayList<>();
        for (GoodsSum goodsSum : list) {
            Map map = new HashMap();
            map.put("value",goodsSum.getNum());
            map.put("name",goodsSum.getGoodsId());
            list1.add(map);
        }
        return list1;
    }

}
