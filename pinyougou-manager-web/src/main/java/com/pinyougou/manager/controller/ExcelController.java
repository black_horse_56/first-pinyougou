package com.pinyougou.manager.controller;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.common.util.ImportExcelUtil;
import com.pinyougou.pojo.TbItemCat;
import com.pinyougou.pojo.TbTypeTemplate;
import com.pinyougou.sellergoods.service.ExcelService;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

@RestController
@RequestMapping("/import")
public class ExcelController {

    @Reference
    private ExcelService excelService;

    /**
     * 方法作用：将 excel 导入到数据库  第一种方式
     *
     * @return
     */
    @RequestMapping("/excelToDb")
    public Result importDb(MultipartFile file, @RequestParam String type) {

        try {
            String originalFilename = file.getOriginalFilename(); // xxxx.xlsx
            InputStream is = file.getInputStream(); // 获取文件的流，需要修改spring-mvc

            List <List <Object>> list = ImportExcelUtil.getBankListByExcel(is, originalFilename);

            excelService.importDb(list, type);

            return new Result(true, "文件导入成功！");

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "文件导入失败！");
        }

    }


    /**
     * 方法作用：将 excel 导入到数据库  第二种方式
     *
     * @return
     */
    @RequestMapping("/excelToDb2")
    public Result importDb2(MultipartFile file, @RequestParam String type) {
        try {

            ExcelReader reader = ExcelUtil.getReader(file.getInputStream());

            if ("type_template".equals(type)) {
                // 表头对应数据库字段
                reader.addHeaderAlias("模版名称", "name")
                        .addHeaderAlias("关联规格", "specIds")
                        .addHeaderAlias("关联品牌", "brandIds")
                        .addHeaderAlias("自定义属性", "customAttributeItems");

                List <TbTypeTemplate> typeTemplates = reader.readAll(TbTypeTemplate.class);

                excelService.importDb2(typeTemplates);
            }

            if ("item_cat".equals(type)) {
                reader.addHeaderAlias("父类目id", "parentId")
                        .addHeaderAlias("类目名称", "name")
                        .addHeaderAlias("类型id", "typeId");

                List <TbItemCat> itemCats = reader.readAll(TbItemCat.class);

                excelService.importDb3(itemCats);
            }
            return new Result(true, "文件导入成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "文件导入失败！");
        }

    }


    /**
     * 方法作用：导出数据成 excel 第一种
     * 对应了导出 订单查询 数据
     *
     * @return
     */
    @RequestMapping("/dbToExcel")
    public Result importExcel() {
        try {
            excelService.importExcel();
            return new Result(true, "文件导出成功，请查看桌面！");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "文件导出失败！");
        }
    }


    /**
     * 方法作用：导出数据成 excel  第二种
     *
     * @return
     */
    @RequestMapping("/dbToExcel2")
    public Result importExcel2() {
        try {
            excelService.importExcel2();
            return new Result(true, "文件导出成功，请查看桌面！");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "文件导出失败！");
        }
    }

}
