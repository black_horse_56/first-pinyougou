package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.user.service.UserService;
import entity.PageResult;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/user")
public class UserController {

    @Reference
    private UserService userService;

    /**
     * 获取用户列表，带分页功能
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @RequestMapping("/getUserList")
    public PageResult getUserList(@RequestBody TbUser user, int pageNumber, int pageSize) {

        return userService.search(user, pageNumber, pageSize);

    }

    /**
     * 获取用户个数、活跃用户个数、非活跃用户个数
     * 活跃用户个数通过判断，3天未登录过即为非活跃用户个数
     *
     * @return
     */
    @RequestMapping("/getUserCountFromStatus")
    public Map getUserCountFromStatus() {
        return userService.getUserCountFromStatus();
    }


    /**
     * 手动冻结用户功能
     * @param userId 用户的ID
     * @return
     */
    @RequestMapping("/frozenUser")
    public Result frozenUser(@RequestParam Long userId) {

        try {
            userService.frozenUser(userId);
            return new Result(true, "冻结成功");

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "冻结失败");
        }

    }


}
