package com.pinyougou.order.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pinyougou.mapper.TbAddressMapper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbUserMapper;
import com.pinyougou.order.service.UserOrderService;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderExample;
import com.pinyougou.pojo.TbOrderExample.Criteria;
import com.pinyougou.pojo.TbUser;
import entity.PageResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class UserOrderServiceImpl implements UserOrderService {

    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private TbUserMapper userMapper;
    @Autowired
    private TbAddressMapper addressMapper;
    @Override
    public PageResult findUserOrder(String userId, int page, int rows) {
          PageHelper.startPage(page, rows);

        //List<TbOrder> tbOrders = tbOrderMapper.selectByExample(null);
        TbOrderExample example=new TbOrderExample();
        Criteria criteria = example.createCriteria();
         criteria.andUserIdEqualTo(userId);
        List<TbOrder> tbOrders = tbOrderMapper.selectByExample(example);

       /* Page<TbOrder>info=( Page<TbOrder>)tbOrderMapper.selectByExample(example);*/
        //设置分页信息
        PageInfo<TbOrder>info=new PageInfo<>(tbOrders);

        return new PageResult(info.getTotal(), info.getList());
    }

    @Override
    public void add(TbUser user) {
        user.setCreated(new Date());
        user.setUpdated(new Date());
        userMapper.insert(user);
    }

    @Override
    public PageResult findUserOrderStatus(String userId, int page, int rows) {

        TbOrder order =new TbOrder();
        order.setUserId(userId);
        order.setStatus("1");
        PageHelper.startPage(page, rows);
        TbOrderExample example=new TbOrderExample();
        Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(order.getUserId());
        criteria.andStatusEqualTo(order.getStatus());
        List<TbOrder> tbOrders = tbOrderMapper.selectByExample(example);
        //设置分页信息
        PageInfo<TbOrder>info=new PageInfo<>(tbOrders);
        return new PageResult(info.getTotal(),info.getList());
    }

/*    @Override
    public UserAndOrdess findUserAndOrdess(String username) {
        //1.通过登录用户名查询tbuser表数据
        TbUserExample example=new TbUserExample();
        TbUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameEqualTo(username);
        userMapper.selectByExample(example);
        //2.通过登录用户名查询地址集合List<TbAddress>address
        TbAddressExample example1=new TbAddressExample();
        TbAddressExample.Criteria criteria1 = example1.createCriteria();
        criteria1.andUserIdEqualTo(username);
        List<TbAddress> tbAddresses = addressMapper.selectByExample(example1);//地址集合
        for (TbAddress address :tbAddresses){
            //3.遍历List<TbAddress> tbAddresses得到地址对象再获取通过得到地址对象再获取省province_id通过province_id查询省
            String provinceId = address.getProvinceId();
        }

        //5.province_id表数据得到province_id通过查询市集合List<Tbcitys>citys
        //6.遍历List<Tbcitys>citys 得到Tbcitys对象获得cityId通过cityId得到区Tbareas

        return null;
    }*/


}
