package com.pinyougou.sellergoods.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.date.DateUtil;
import com.pinyougou.mapper.SaleMapper;
import com.pinyougou.pojogroup.QueryVo;
import com.pinyougou.pojogroup.SellData;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderExample;
import com.pinyougou.pojo.TbOrderExample.Criteria;
import com.pinyougou.sellergoods.service.OrderService;

import entity.PageResult;

/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private TbOrderMapper orderMapper;


    /**
     * 查询全部
     */
    @Override
    public List<TbOrder> findAll() {
        return orderMapper.selectByExample(null);
    }


    /**
     * 订单发货查询
     *
     * @param orderId 订单号
     * @return 发货列表
     */
    @Override
    public List<TbOrder> searchById(Long orderId) {
        // select * from tb_order where orderId =?

        TbOrderExample example = new TbOrderExample();
        example.createCriteria().andOrderIdEqualTo(orderId);
        return orderMapper.selectByExample(example);
    }


    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Override
    public void add(TbOrder order) {
        orderMapper.insert(order);
    }


    /**
     * 修改
     */
    @Override
    public void update(TbOrder order) {
        orderMapper.updateByPrimaryKey(order);
    }

    /**
     * 根据ID获取实体
     *
     * @param id id
     * @return 实体
     */
    @Override
    public TbOrder findOne(Long id) {
        return orderMapper.selectByPrimaryKey(id);
    }


    @Override
    public PageResult findPage(TbOrder order, int pageNum, int pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        TbOrderExample example = new TbOrderExample();
        Criteria criteria = example.createCriteria();

        if (order != null) {
            if (order.getStatus()!=null&&order.getStatus().length()>0) {
                criteria.andStatusLike("%" + order.getStatus() + "%");
            }
            if (order.getUserId()!=null&&order.getUserId().length()>0){
                criteria.andUserIdLike("%"+order.getUserId()+"%");
            }
        }

        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private SaleMapper saleMapper;

    /**
     * 获取销售数据，把它存入 Map 集合
     *
     * @param queryVo 查询
     * @return map
     */
    @Override
    public Map<String, Object> findSellData(QueryVo queryVo) {
        //创建一个Map集合用于返回数据
        Map<String, Object> map = new HashMap<>();
        //调用SaleMapper进行销售数据的查询
        List<SellData> list = saleMapper.findByQueryVo(queryVo);
        //创建一个List集合
        List<String> categoryList = new ArrayList<>();
        List<Object> dataList = new ArrayList<>();
        //循环遍历数组
        for (SellData data : list) {
            categoryList.add(DateUtil.format(data.getCreateTime(),"yyyy-MM-dd"));
            dataList.add(data.getTotalPayment());
        }
        map.put("categories",categoryList);
        map.put("data",dataList);
        return map;
    }

}
