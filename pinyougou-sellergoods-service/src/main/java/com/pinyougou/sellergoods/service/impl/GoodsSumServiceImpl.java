package com.pinyougou.sellergoods.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.GoodsSumDao;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.sellergoods.service.GoodsSumService;

import entity.GoodsSum;
import entity.PageResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsSumServiceImpl implements GoodsSumService {

    //a
    @Autowired
    private GoodsSumDao goodsSumDao;

    @Autowired
    private TbOrderItemMapper orderItemMapper;


    @Override
    public PageResult findPage(long time, int pageNum, int pageSize) {

        PageHelper.startPage(pageNum, pageSize);

        ArrayList<GoodsSum> allGoods= new ArrayList<>();

        if (time==1) {
             allGoods =goodsSumDao.findAllGoods();
        }else {
            long l = time;
            Date date1 = new Date(l);
            System.out.println(date1);
             allGoods=goodsSumDao.findByTime(date1);
        }

        Page<GoodsSum> page = (Page<GoodsSum>) allGoods;


        return new PageResult(page.getTotal(), page.getResult());

    }

    @Override
    public List<GoodsSum> findAll(long time) {
        ArrayList<GoodsSum> allGoods= new ArrayList<>();

        if (time==1) {
            allGoods =goodsSumDao.findAllGoods();
        }else {
            long l = time;
            Date date1 = new Date(l);
            System.out.println(date1);
            allGoods=goodsSumDao.findByTime(date1);
        }

        return allGoods;
    }
}
