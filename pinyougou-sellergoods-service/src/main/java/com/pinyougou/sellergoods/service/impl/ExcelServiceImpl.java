package com.pinyougou.sellergoods.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.*;
import com.pinyougou.pojo.*;
import com.pinyougou.sellergoods.service.ExcelService;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.*;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    private TbBrandMapper brandMapper;

    @Autowired
    private TbSpecificationMapper specificationMapper;

    @Autowired
    private TbTypeTemplateMapper typeTemplateMapper;

    @Autowired
    private TbItemCatMapper itemCatMapper;

    @Autowired
    private TbOrderMapper orderMapper;

    /**
     * excel 表 导入 数据库  第一种方式
     *
     * @param list
     */
    public void importDb(List <List <Object>> list, String type) {

        // 数据格式  [[ abc, A], [叶子, D]]

        if ("brand".equals(type)) {
            toBrand(list);
        }

        if ("specification".equals(type)) {
            toSpecification(list);
        }
    }

    /**
     * 导入到 specification 表  第一种方式
     *
     * @param list
     */
    private void toSpecification(List <List <Object>> list) {
        // 循环遍历
        for (int i = 0; i < list.size(); i++) {
            TbSpecification tbSpecification = new TbSpecification();
            // 每个单元格的数据
            for (int j = 0; j < list.get(i).size(); j++) {
                tbSpecification.setSpecName(list.get(i).get(0) + "");
            }
            specificationMapper.insert(tbSpecification);
        }
    }

    /**
     * 导入到 brand表  第一种方式
     *
     * @param list
     */
    private void toBrand(List <List <Object>> list) {
        // 循环遍历
        for (int i = 0; i < list.size(); i++) {
            TbBrand brand = new TbBrand();
            // 每个单元格的数据
            for (int j = 0; j < list.get(i).size(); j++) {
                brand.setName(list.get(i).get(0) + "");
                brand.setFirstChar(list.get(i).get(1) + "");
            }
            brandMapper.insert(brand);
        }
    }


    /**
     * excel 表 导入 数据库  第二种方式
     *
     * @param typeTemplates 导入的表
     */
    @Override
    public void importDb2(List <TbTypeTemplate> typeTemplates) {
        for (TbTypeTemplate typeTemplate : typeTemplates) {
            typeTemplateMapper.insert(typeTemplate);
        }
    }


    /**
     * excel 表 导入 数据库  第二种方式
     *
     * @param itemCats 导入的表
     */
    @Override
    public void importDb3(List <TbItemCat> itemCats) {
        for (TbItemCat itemCat : itemCats) {
            itemCatMapper.insert(itemCat);
        }
    }


    /**
     * 导出数据到excel表  第一种方式
     */
    public void importExcel() throws Exception {

        List <TbBrand> brands = brandMapper.selectByExample(null);

        // 在内存中创建一个Excel文件，通过输出流写到客户端提供下载
        // 内存中保留 10000 条数据，以免内存溢出，其余写入 硬盘
        SXSSFWorkbook workbook = new SXSSFWorkbook(10000);

        // 创建一个sheet页
        SXSSFSheet sheet = workbook.createSheet("brand");
        // 创建标题
        SXSSFRow headRow = sheet.createRow(0);
        headRow.createCell(0).setCellValue("id");
        headRow.createCell(1).setCellValue("品牌名称");
        headRow.createCell(2).setCellValue("品牌首字母");

        for (TbBrand brand : brands) {
            // 创建行
            SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
            dataRow.createCell(0).setCellValue(brand.getId());
            dataRow.createCell(1).setCellValue(brand.getName());
            dataRow.createCell(2).setCellValue(brand.getFirstChar());
        }

        // 设置Excel文件名，并以中文进行编码
        // String codedFileName = new String("销售订单".getBytes("gbk"), "iso-8859-1");

        // 获取桌面的绝对路径
        String desktop = FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath();
        FileOutputStream fos = new FileOutputStream(new File(desktop + "\\" + new Date().getTime() + ".xlsx"));

        // 将指定的字节写入此输出流
        workbook.write(fos);
        // 刷新此输出流并强制将所有缓冲的输出字节被写出
        fos.flush();
        // 关闭流
        fos.close();
        // 处理在磁盘上备份此工作簿的临时文件SXSSF分配临时文件，必须始终清除显式，通过调用dispose方法
        workbook.dispose();
    }


    /**
     * 导出数据到excel表  第二种方式
     */
    public boolean importExcel2() {

        List <TbOrder> orders = orderMapper.selectByExample(null);

        String[] status =
                {"未付款", "已付款", "未发货", "已发货", "交易成功", "交易关闭", "待评价"};

        String[] paymentType = {"在线支付", "货到付款"};

        List <Map <String, Object>> rows = new ArrayList <>();

        for (TbOrder order : orders) {
            Map <String, Object> map = new LinkedHashMap <>();
            map.put("订单id", order.getOrderId());
            map.put("用户名称", order.getUserId());
            map.put("商家名", order.getSellerId());
            map.put("下单时间", order.getCreateTime());
            map.put("付款方式", paymentType[Integer.parseInt(order.getPaymentType())]);
            map.put("价格", order.getPayment());
            map.put("状态", status[Integer.parseInt(order.getStatus())]);
            List <Map <String, Object>> maps = CollUtil.newArrayList(map);
            rows.addAll(maps);
        }

        // 获取桌面的绝对路径
        String desktop = FileSystemView.getFileSystemView().getHomeDirectory().getAbsolutePath();
        ExcelWriter writer = ExcelUtil.getWriter(
                desktop + "\\" + new Date().getTime() + ".xlsx");
        writer.write(rows);
        writer.close();

        return true;
    }

}

