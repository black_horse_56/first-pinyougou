package com.pinyougou.user.service.impl;

import java.util.*;
import java.util.concurrent.TimeUnit;

import com.alibaba.dubbo.qos.command.impl.Online;
import com.github.pagehelper.PageInfo;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.pojo.TbBrandExample;
import com.pinyougou.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbUserMapper;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.pojo.TbUserExample;
import com.pinyougou.pojo.TbUserExample.Criteria;


import entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.jms.Destination;

/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private TbUserMapper userMapper;

    /**
     * 查询全部
     */
    @Override
    public List <TbUser> findAll() {
        return userMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page <TbUser> page = (Page <TbUser>) userMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Override
    public void add(TbUser user) {
        user.setCreated(new Date());
        user.setUpdated(user.getCreated());
        //加密存储
        String md5passowrd = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5passowrd);
        userMapper.insert(user);
    }


    /**
     * 修改
     */
    @Override
    public void update(TbUser user) {
        userMapper.updateByPrimaryKey(user);
    }

    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbUser findOne(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            userMapper.deleteByPrimaryKey(id);
        }
    }


    @Override
    public PageResult findPage(TbUser user, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbUserExample example = new TbUserExample();
        Criteria criteria = example.createCriteria();

        if (user != null) {
            if (user.getUsername() != null && user.getUsername().length() > 0) {
                criteria.andUsernameLike("%" + user.getUsername() + "%");
            }
            if (user.getPassword() != null && user.getPassword().length() > 0) {
                criteria.andPasswordLike("%" + user.getPassword() + "%");
            }
            if (user.getPhone() != null && user.getPhone().length() > 0) {
                criteria.andPhoneLike("%" + user.getPhone() + "%");
            }
            if (user.getEmail() != null && user.getEmail().length() > 0) {
                criteria.andEmailLike("%" + user.getEmail() + "%");
            }
            if (user.getSourceType() != null && user.getSourceType().length() > 0) {
                criteria.andSourceTypeLike("%" + user.getSourceType() + "%");
            }
            if (user.getNickName() != null && user.getNickName().length() > 0) {
                criteria.andNickNameLike("%" + user.getNickName() + "%");
            }
            if (user.getName() != null && user.getName().length() > 0) {
                criteria.andNameLike("%" + user.getName() + "%");
            }
            if (user.getStatus() != null && user.getStatus().length() > 0) {
                criteria.andStatusLike("%" + user.getStatus() + "%");
            }
            if (user.getHeadPic() != null && user.getHeadPic().length() > 0) {
                criteria.andHeadPicLike("%" + user.getHeadPic() + "%");
            }
            if (user.getQq() != null && user.getQq().length() > 0) {
                criteria.andQqLike("%" + user.getQq() + "%");
            }
            if (user.getIsMobileCheck() != null && user.getIsMobileCheck().length() > 0) {
                criteria.andIsMobileCheckLike("%" + user.getIsMobileCheck() + "%");
            }
            if (user.getIsEmailCheck() != null && user.getIsEmailCheck().length() > 0) {
                criteria.andIsEmailCheckLike("%" + user.getIsEmailCheck() + "%");
            }
            if (user.getSex() != null && user.getSex().length() > 0) {
                criteria.andSexLike("%" + user.getSex() + "%");
            }

        }

        Page <TbUser> page = (Page <TbUser>) userMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 带搜索功能的用户查询
     *
     * @param user
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public PageResult search(TbUser user, int pageNumber, int pageSize) {

        PageHelper.startPage(pageNumber, pageSize);

        TbUserExample example = new TbUserExample();

        TbUserExample.Criteria criteria = example.createCriteria();

        if (user != null) {
            if (StringUtils.isNotBlank(user.getUsername())) {
                criteria.andUsernameLike("%" + user.getUsername() + "%");
            }

        }

        Page <TbUser> page = (Page <TbUser>) userMapper.selectByExample(example);

        return new PageResult(page.getTotal(), page.getResult());
    }


    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private JmsTemplate jmsTemplate;
    @Resource(name = "pinyougou_sms")
    private Destination destination;

    @Override
    public boolean createSmsCode(String phone) {
        //1.生成短信验证码  6位数字
        double random = Math.random() * 1000000;
        String code = (long) random + "";

        //2.保存验证码 到redis中  key
        redisTemplate.boundValueOps("USER_REGISTER_" + phone).set(code, 24, TimeUnit.HOURS);

        //3.发送消息 将数据发送到短信平台
        Map <String, String> map = new HashMap <>();
        map.put("mobile", phone);
        map.put("template_code", "SMS_126865257");
        map.put("sign_name", "黑马三国的包子");
        map.put("param", "{\"code\":\"" + code + "\"}");
        jmsTemplate.convertAndSend(destination, map);
        return true;
    }

    @Override
    public boolean isChecked(String phone, String code) {

        //1.先根据用户的手机号获取redis中的验证码
        String coderedis = (String) redisTemplate.boundValueOps("USER_REGISTER_" + phone).get();
        if (coderedis == null) {
            return false;
        }
        //2.匹配页面传递过来是否一致，如果一致就是正确

        if (coderedis.equals(code)) {
            return true;
        }

        return false;
    }

    /**
     * 返回为Map集合，键代表  0=用户数  1=活跃用户数  2=非活跃用户数
     * 活跃用户个数通过判断
     *
     * @return
     */
    @Override
    public Map getUserCountFromStatus() {

        Map <String, Integer> map = new HashMap <>();

        List <TbUser> users = userMapper.selectByExample(null);

        // 用户数
        map.put("userCount", users.size());

        // 活跃用户数

        for (TbUser user : users) {
            // 可能存在用户注册了但是没有登录过系统的情况，
            // 也可以在用户首次注册的验证通过后给一个最后登录时间
            // 当前测试下直接跳过
            if (user.getLastLoginTime() == null){
                continue;
            }
            long time = new Date().getTime() - user.getLastLoginTime().getTime();
            long l = time / (24 * 3600 * 1000);
            if (l > 90) {
                // 查询时如果用户大于90天没登录，冻结用户
                frozenUser(user.getId());
            }
        }

        int activeUserCount = 0;

        Set keys = redisTemplate.boundHashOps("activeUser").keys();
        for (Object key : keys) {
            Integer activeUser = (Integer) redisTemplate.boundHashOps("activeUser").get(key);
            if (activeUser > 1) {
                activeUserCount++;
            }
        }

        // 在线用户人数
        int onlineUserCount = redisTemplate.boundHashOps("onlineUser").keys().size();

        map.put("activeUserCount", activeUserCount);
        map.put("unactiveUserCount", users.size() - activeUserCount);
        map.put("onlineUserCount", onlineUserCount);

        return map;
    }

    /**
     * 手动冻结用户功能
     *
     * @param userId
     */
    @Override
    public void frozenUser(Long userId) {

        TbUser tbUser = userMapper.selectByPrimaryKey(userId);

        tbUser.setStatus("N");

        userMapper.updateByPrimaryKeySelective(tbUser);

    }

    /**
     * 用户登录了就存储到redis中去
     *
     * @param username
     */
    @Override
    public boolean updateUserStatus(String username) {

        TbUserExample example = new TbUserExample();
        example.createCriteria().andUsernameEqualTo(username);
        // 正常情况用户名不会重复
        TbUser user = userMapper.selectByExample(example).get(0);

        //判断用户的lastlogintime，如果为null，判定为首次登陆，赋予一个当前值
        if (user.getLastLoginTime() == null) {
            user.setLastLoginTime(new Date());
        }

        // 如果 状态为 N 表示已经冻结，拒绝登录
        if ("N".equals(user.getStatus())) {
            return false;
        }

        // 用户3个月未登录，冻结用户，拒绝登录
        long time = (new Date().getTime() - user.getLastLoginTime().getTime()) / (24 * 3600 * 1000);

        if (time > 90) {
            // 修改数据库状态
            frozenUser(user.getId());
            return false;
        }

        Object isOnline = redisTemplate.boundHashOps("onlineUser").get(username);

        // 用户可以登录 且是本次登录的首次授权 更新最后登录时间
        if (isOnline == null) {
            user.setLastLoginTime(new Date());
            user.setStatus("Y");
            userMapper.updateByPrimaryKeySelective(user);
        }


        redisTemplate.boundHashOps("onlineUser").put(username, username);

        return true;
    }

    /**
     * 用户访问controller的记录，鉴别活跃用户统计
     *
     * @param username
     */
    @Override
    public void visit(String username) {

        if (redisTemplate.boundHashOps("activeUser").get(username) == null) {
            redisTemplate.boundHashOps("activeUser").put(username, 1);
        } else {
            Integer count = (Integer) redisTemplate.boundHashOps("activeUser").get(username);
            redisTemplate.boundHashOps("activeUser").put(username, count + 1);
        }

    }

    /**
     * 该方法没有用到
     *
     * @param username
     */
    @Override
    public void userOffline(String username) {
        System.out.println(redisTemplate.boundHashOps("activeUser").keys().size());
        System.out.println(redisTemplate.boundHashOps("onlineUser").keys().size());

        redisTemplate.boundHashOps("activeUser").delete(username);
        redisTemplate.boundHashOps("onlineUser").delete(username);

        System.out.println(redisTemplate.boundHashOps("activeUser").keys().size());
        System.out.println(redisTemplate.boundHashOps("onlineUser").keys().size());
    }

    public static void main(String[] args) {
        double random = Math.random() * 1000000;
        System.out.println((long) random);
    }

}
