package com.pinyougou.search.listener;


import com.alibaba.fastjson.JSON;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.search.service.ItemSearchService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;

public class UpdateIndexListener implements MessageListener {

    @Autowired
    private ItemSearchService itemSearchService;

    @Override
    public void onMessage(Message message) {
        //判断消息是不是字符串对象
        if (message instanceof TextMessage){
             try {
                //转换消息类型，并接收消息
                TextMessage textMessage=(TextMessage)message;
                String text = textMessage.getText();
                //把消息转换成List<TbItem>集合
                List<TbItem> itemList = JSON.parseArray(text, TbItem.class);
                //调用搜索服务完成功能
                itemSearchService.updateIndex(itemList);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }

    }
}
