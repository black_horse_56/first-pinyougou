package com.pinyougou.search.listener;

import com.pinyougou.search.service.ItemSearchService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

public class DeleteIndexListener implements MessageListener {

    @Autowired
    private ItemSearchService itemSearchService;

    @Override
    public void onMessage(Message message) {
        //判断消息是不是对象类型
        if (message instanceof ObjectMessage){
            try {
                //转换消息类型并接收消息
                ObjectMessage objectMessage=(ObjectMessage)message;
                Long[] ids = (Long[]) objectMessage.getObject();
                //调用搜索服务完成功能
                itemSearchService.deleteByIds(ids);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }

    }
}
