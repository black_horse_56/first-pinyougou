package com.pinyougou.order.service;

import com.pinyougou.pojo.TbUser;
import entity.PageResult;

public interface UserOrderService {
    PageResult findUserOrder(String userId, int page, int rows);


    void add(TbUser user);

    PageResult findUserOrderStatus(String userId, int page, int rows);

    /*UserAndOrdess findUserAndOrdess(String username);*/
}
