app.controller('itemController',function($http,$scope){
	//定义一个变量，用来实现订单数量的增减
	$scope.num=1;
	//定义一个方法，当页面点击的时候，改变变量的值
	$scope.addNum=function(value){
		//判断$scope.num是否是数字类型
		if(isNaN($scope.num)){
			$scope.num=1;
			return;
		}
		//将$scope.num,value转换成int类型
		$scope.num=parseInt($scope.num);
		value=parseInt(value);
		$scope.num=$scope.num+value;
		if($scope.num<=0){
			$scope.num=1;
		}
	}
	

	//定义一个方法,当页面点击的时候用来存储数据
	$scope.selectSpecification=function(name,value){
		$scope.specificationItems[name]=value;
		$scope.search();
	}
	
	//定义一个方法用来判断规格选项是否被选中
	$scope.isSelected=function(name,value){
		if($scope.specificationItems[name]==value){
			return true;
		}else{
			return false;
		}
	}
	
	//定义一个变量用来保存skuList中的第一个对象的值
	$scope.sku=skuList[0];
	
	//获取skuList中的数据，把一个skuList中的第一个变量中的spec属性值赋值给specificationItems
	$scope.specificationItems=angular.fromJson(angular.toJson(skuList[0].spec));//浅克隆 变成深克隆
	
	//定义一个方法  用于  循环遍历SKU的数组  判断 点击到的规格的对象 是否在 SKU的数组中存在，
	//如果存在，将SKU的对象赋值给￥scope.sku  该变量双向绑定到了页面，
	
	$scope.search=function(){
		
		for(var i=0;i<skuList.length;i++){
			var obj = skuList[i];//  {'id':10561623,'price':0.01,'title':'华为P30手机56 移动3G 16G',spec:{"网络":"移动3G","机身内存":"16G"}
			
			console.log(angular.toJson(obj.spec));
			console.log(angular.toJson($scope.specificationItems));
			
			if(angular.toJson(obj.spec)==angular.toJson($scope.specificationItems)){
				$scope.sku=obj;
				break;
			}
		}
	}

	//定义一个方法 用来添加商品到购物车
	$scope.addToCart=function () {
		$http.get('http://localhost:9107/cart/addGoodsToCartList.do?itemId='+$scope.sku.id+'&num='+$scope.num,{'withCredentials':true}).success(
			function (response) {
				if (response.success){
					//添加成功，跳转到购物车页面
					window.location.href='http://localhost:9107/cart.html';
				}else{
					alert(response.message);
				}
            }
		)
    }



	
	
})