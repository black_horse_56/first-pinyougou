package com.pinyougou.seckill.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.common.util.SysConstants;
import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import com.pinyougou.seckill.service.SeckillGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.pinyougou.seckill.service.impl *
 * @since 1.0
 */
@Service
public class SeckillGoodsServiceImpl implements SeckillGoodsService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;


    @Override
    public List<TbSeckillGoods> findAll() {
        //从redis中获取所有的商品的数据
        List<TbSeckillGoods> goods = redisTemplate.boundHashOps(SysConstants.SEC_KILL_GOODS_REDIS_KEY).values();
        return goods;
    }

    @Override
    public TbSeckillGoods findOne(Long id) {
        return (TbSeckillGoods) redisTemplate.boundHashOps(SysConstants.SEC_KILL_GOODS_REDIS_KEY).get(id);
    }

    /**此方法用于保存数据到Redis中
     * @param ids
     */
    @Override
    public void saveDataToRedis(Long[] ids) {
        //商品的列表存储redis中
        TbSeckillGoodsExample example=new TbSeckillGoodsExample();
        TbSeckillGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(Arrays.asList(ids)).andStockCountGreaterThan(0)
                .andStartTimeLessThan(new Date()).andEndTimeGreaterThan(new Date());
        List<TbSeckillGoods> goodsList = seckillGoodsMapper.selectByExample(example);
        for (TbSeckillGoods seckillGoods : goodsList) {
            Integer stockCount = seckillGoods.getStockCount();
            for (Integer i = 0; i < stockCount; i++) {
                redisTemplate.boundListOps(SysConstants.SEC_KILL_GOODS_PREFIX+seckillGoods.getId()).leftPush(seckillGoods.getId());
            }
            redisTemplate.boundHashOps(SysConstants.SEC_KILL_GOODS_REDIS_KEY).put(seckillGoods.getId(),seckillGoods);
        }
    }
}
