package com.pinyougou.page.listener;

import com.pinyougou.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;


public class GenHtmlListener implements MessageListener {

    @Autowired
    private ItemPageService itemPageService;

    @Override
    public void onMessage(Message message) {
        //判断消息是不是一个序列化的java对象
         if (message instanceof ObjectMessage){
             try {
                 //转换消息类型，并接收消息
                 ObjectMessage objectMessage=(ObjectMessage)message;
                 Long[] ids = (Long[]) objectMessage.getObject();
                 //循环遍历ids数组,调用静态服务完成功能
                 for (Long id : ids) {
                     itemPageService.genHtml(id);
                 }
             } catch (JMSException e) {
                 e.printStackTrace();
             }
         }

    }
}
