package com.pinyougou.page.listener;

import com.pinyougou.page.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class DeleteHtmlListener implements MessageListener {

    @Autowired
    private ItemPageService itemPageService;

    @Override
    public void onMessage(Message message) {
        //判断消息是不是对象类型
        if (message instanceof ObjectMessage){
            try {
                //转换消息类型并接收消息
                ObjectMessage objectMessage=(ObjectMessage)message;
                Long[] ids = (Long[]) objectMessage.getObject();
                //调用静态服务完成功能
                for (Long id : ids) {
                    itemPageService.deleteHtml(id);
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }

    }
}
