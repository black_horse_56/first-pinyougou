package com.pinyougou.page.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.TbGoodsDescMapper;
import com.pinyougou.mapper.TbGoodsMapper;
import com.pinyougou.mapper.TbItemCatMapper;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.page.service.ItemPageService;
import com.pinyougou.pojo.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemPageServiceImpl implements ItemPageService {

    @Autowired
    private FreeMarkerConfigurer configurer;

    @Autowired
    private TbGoodsMapper goodsMapper;

    @Autowired
    private TbGoodsDescMapper goodsDescMapper;

    @Autowired
    private TbItemMapper itemMapper;

    @Autowired
    private TbItemCatMapper itemCatMapper;

    @Value("${PageDir}")
    private String PageDir;

    /**根据SPU id 生成静态页面
     * @param id
     */
    @Override
    public void genHtml(Long id) {
        //1.调用goodsMapper和goodsDescMapper查询SPU id所对应的SPU信息
        TbGoods goods = goodsMapper.selectByPrimaryKey(id);
        TbGoodsDesc goodsDesc = goodsDescMapper.selectByPrimaryKey(id);
        //2.调用freemarker完成静态页面的生成
        genhtml("item.ftl",goods,goodsDesc);
    }

    private void genhtml(String templateName, TbGoods goods, TbGoodsDesc goodsDesc) {
        FileWriter fileWriter=null;
        try {
            //获取Configuration对象
            Configuration configuration = configurer.getConfiguration();
            //获取模板对象
            Template template = configuration.getTemplate(templateName);
            //创建一个Map集合用来存储数据
            Map<String, Object> map = new HashMap<>();
            map.put("goods",goods);
            map.put("goodsDesc",goodsDesc);
            //调用itemCatMapper查询一级/二级/三级分类的名称
            TbItemCat tbItemCat1 = itemCatMapper.selectByPrimaryKey(goods.getCategory1Id());
            TbItemCat tbItemCat2 = itemCatMapper.selectByPrimaryKey(goods.getCategory2Id());
            TbItemCat tbItemCat3 = itemCatMapper.selectByPrimaryKey(goods.getCategory3Id());
            map.put("tbItemCat1",tbItemCat1.getName());
            map.put("tbItemCat2",tbItemCat2.getName());
            map.put("tbItemCat3",tbItemCat3.getName());
            //根据SPU id查询相关联的SKU数据
            TbItemExample example=new TbItemExample();
            TbItemExample.Criteria criteria = example.createCriteria();
            criteria.andGoodsIdEqualTo(goods.getId()).andStatusEqualTo("1");
            example.setOrderByClause("is_default desc");
            List<TbItem> skuList = itemMapper.selectByExample(example);
            map.put("skuList",skuList);
            //创建一个写流，用来关联输出的文件路径
            fileWriter = new FileWriter(PageDir + goods.getId() + ".html");
            //调用Template对象的process()方法生成静态页面
            template.process(map,fileWriter);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileWriter!=null){
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**根据SPU id删除其静态页面
     * @param id
     */
    public void deleteHtml(Long id){
        //创建File对象(用于关联要删除的静态页面)
        File file = new File(PageDir + id + ".html");
        file.delete();
    }


}
