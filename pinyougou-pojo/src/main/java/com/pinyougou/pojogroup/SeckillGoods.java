package com.pinyougou.pojogroup;

import com.pinyougou.pojo.TbSeckillGoods;

import java.io.Serializable;

public class SeckillGoods implements Serializable {
    private TbSeckillGoods seckillGoods;

    public TbSeckillGoods getSeckillGoods() {
        return seckillGoods;
    }

    public void setSeckillGoods(TbSeckillGoods seckillGoods) {
        this.seckillGoods = seckillGoods;
    }
}
