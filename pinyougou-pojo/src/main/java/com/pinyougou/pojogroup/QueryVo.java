package com.pinyougou.pojogroup;

import java.io.Serializable;

public class QueryVo implements Serializable {

    private String sellerId;
    private Long timeInterval;

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Long getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Long timeInterval) {
        this.timeInterval = timeInterval;
    }
}









