package com.pinyougou.pojogroup;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SellData implements Serializable {

    private BigDecimal totalPayment;
    private Date createTime;

    public BigDecimal getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(BigDecimal totalPayment) {
        this.totalPayment = totalPayment;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
