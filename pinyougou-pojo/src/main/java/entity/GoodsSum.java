package entity;

import java.io.Serializable;
import java.math.BigDecimal;

//商品销量
public class GoodsSum implements Serializable {

    private  Long goodsId;

    private  Integer  num;

    private BigDecimal totalFee;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public GoodsSum() {
    }

    public GoodsSum(Long goodsId, Integer num, BigDecimal totalFee) {
        this.goodsId = goodsId;
        this.num = num;
        this.totalFee = totalFee;
    }
}
