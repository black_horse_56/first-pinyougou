package com.pinyougou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbAddress;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.user.service.AddressService;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/address")
public class UserAddressController {
    @Reference
    private AddressService addressService;


     //编辑用户地址
       @RequestMapping("/updateAddress")
       public Result updateAddress(@RequestBody TbAddress address){
           try {
               addressService.update(address);
               return new Result(true,"编辑成功");
           } catch (Exception e) {
               e.printStackTrace();
               return  new Result(false,"编辑失败");
           }
       }

       //当点击编辑是展示当前用户的需要编辑的地址
              @RequestMapping("/findOne")
              public TbAddress findOne(Long id){
                 return addressService.findOne(id);
              }


     //展示登录用户地址
    @RequestMapping("/findListByLoginUser")
    public List<TbAddress> findListByLoginUser(){
        //获取登录用户
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        //通过userId和获取用户列表信息
        return addressService.findListByUserId(userId);
    }


  //增加用户地址
    @RequestMapping("/add")
    public Result add(@RequestBody TbAddress address){
        try {
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            address.setUserId(userId);
            addressService.add(address);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "增加失败");
        }
    }


    @RequestMapping("/delete")
    public Result deleteAddress(Long id){

        try {
            addressService.deleteAddress(id);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除失败");
        }
    }

    //修改个人资料
    //1.查询登录个人当加载个人信息页面时信息显示
    @RequestMapping("/findUserMessage")
    public TbUser findUserMessage(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
       return addressService.findUserMessage(username);
    }
    //修改个人资料
    @RequestMapping("/updateUserMessage")
    public Result updateUserMessage(@RequestBody TbUser user){
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            user.setUsername(username);
            addressService.updateUserMessage(user);

            return  new Result(true,"资料修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"资料修改失败");
        }
    }

}
