package com.pinyougou.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.order.service.UserOrderService;
import entity.PageResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userOrder")
public class UserOrderController {

    @Reference
    private UserOrderService userorderService;

    @RequestMapping("/searchUserOrder")
    //参数 page 当前页 ,rows当前页结果集
    public PageResult userOrder(int page, int rows ){
        //1.用户的所有订单==》List<TbOrder>显示到页面

        //1.分页查询==>当前页码,当前页码记录
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();

            //1.通过登录用户查询登录用户的订单对象
        PageResult userOrder = userorderService.findUserOrder(userId, page, rows);

        System.out.println(userOrder.getRows());
        return userorderService.findUserOrder( userId,page,rows);



    }


    //查询展示未支付用户信息
     //分页查询status=1 且登录用户的订单
    @RequestMapping("/searchUserOrderStatus")
    public PageResult userOrderStatus(int page, int rows){
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();

       return userorderService.findUserOrderStatus(userId, page, rows);
    }





}
