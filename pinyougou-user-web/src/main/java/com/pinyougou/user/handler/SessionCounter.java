package com.pinyougou.user.handler;

import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Set;

@Component
public class SessionCounter implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {

        HttpSession session = se.getSession();

        System.out.println(session.getId());
        System.out.println(session.getCreationTime());
        System.out.println(session.getAttributeNames());
        System.out.println("session创建了");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

        HttpSession session = se.getSession();

        ApplicationContext ctx = WebApplicationContextUtils.
                getRequiredWebApplicationContext(session.getServletContext());

        RedisTemplate redisTemplate = (RedisTemplate) ctx.getBean("redisTemplate");

        Object username = session.getAttribute(session.getId());

        if (username != null) {

            Set keys = redisTemplate.boundHashOps("onlineUser").keys();

            for (Object key : keys) {
                String value = redisTemplate.boundHashOps("onlineUser").get(key) + "";
                if (value.equals(username + "")){
                    redisTemplate.boundHashOps("onlineUser").delete(value);
                    redisTemplate.boundHashOps("activeUser").delete(value);
                }
            }

        }

        System.out.println(session.getId());
        System.out.println(session.getCreationTime());
        System.out.println(session.getAttributeNames());
        System.out.println("session销毁了");

    }

}