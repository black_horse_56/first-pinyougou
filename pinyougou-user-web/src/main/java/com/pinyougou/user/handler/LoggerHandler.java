package com.pinyougou.user.handler;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.user.service.UserService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggerHandler {

    @Autowired
    private HttpServletRequest request;

    @Reference
    private UserService userService;

    /***
     * 前置通知日志操作，监控用户的访问controller的记录
     * @param
     */
    @Before(value = "execution(* com.pinyougou.user.controller.*Controller.*(..))")
    public void logBefore() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println(username + "访问了");
        userService.visit(username);
    }


}
