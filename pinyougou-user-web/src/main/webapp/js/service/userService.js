//服务层
app.service('userService',function($http){
	    	
	//读取列表数据绑定到表单中
	this.findAll=function(){
		return $http.get('../user/findAll.do');		
	}
	//分页 
	this.findPage=function(page,rows){
		return $http.get('../user/findPage.do?page='+page+'&rows='+rows);
	}
	//查询实体
	this.findOne=function(id){
		return $http.get('../user/findOne.do?id='+id);
	}
	//增加 
	this.add=function(entity,code){
		return  $http.post('../user/add.do?code='+code,entity );
	}
	//修改 
	this.update=function(entity){
		return  $http.post('../user/update.do',entity );
	}
	//删除
	this.dele=function(ids){
		return $http.get('../user/delete.do?ids='+ids);
	}
	//搜索
	this.search=function(page,rows,searchEntity){
		return $http.post('../user/search.do?page='+page+"&rows="+rows, searchEntity);
	}

	this.createSmsCode=function (phone) {
		return $http.get('../user/createSmsCode.do?phone='+phone);
    }













    //用户订单分页信息
    this.userOrder=function (page,rows) {

        return $http.get('/userOrder/searchUserOrder.do?page='+page+"&rows="+rows)

    }
    /*    //用户未支付订单分页信息

      this.userOrderStatus=function (page,rows) {

          return $http.get('/userOrder/searchUserOrderStatus.do?page='+page+"&rows="+rows)

      }*/
    //基本资料
    //1.登录用户个人基本资料展示
    this.findUserMessage=function () {
        return $http.get('/address/findUserMessage.do')
    };
    //2修改基本资料
    this.updateUserMessage=function (entity) {
        return $http.post("/address/updateUserMessage.do",entity)
    }




    //查询展示用户地址信息
    this.findAddress=function () {
        return $http.post('/address/findListByLoginUser.do')
    }

    //新增用户地址
    this.addAddress=function (userAddress) {
        return $http.post('/address/add.do',userAddress)
    }



    //查询用户实体对象
    this.findOneAddress=function (id) {
        return $http.get('/address/findOne.do?id='+id)
    }
    //修改用户地址信息
    this.updateAddress=function (address) {
        return $http.post('/address/updateAddress.do',address)
    }

    //删除用户地址
    this.deleteAddress=function (id) {
        return $http.get('/address/delete.do?id='+id)
    }
});
