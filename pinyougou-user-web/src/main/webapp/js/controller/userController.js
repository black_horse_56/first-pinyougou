 //控制层 
app.controller('userController' ,function($scope,$controller,userService){

    $controller('baseController',{$scope:$scope});//继承
	//写一个方法  当点击注册的时候调用 发送请求 将数据存储到数据库

	$scope.register=function () {
		//先进行校验
		if($scope.entity.password!=$scope.confirmPassword){
			alert("密码要一致");
			return;
		}
        userService.add($scope.entity,$scope.code).success(
        	function (response) {//result
				if(response.success){
					//要跳转到登录的页面
					alert("要登录")
				}else{
					alert(response.message);

				}
            }
		)
    }

    $scope.createSmsCode=function () {
		userService.createSmsCode($scope.entity.phone).success(
			function (response) {//result
				alert(response.message);
            }
		)
    }









    //定义一个方法当登录时显示用户名
    $scope.showName=function () {
        userService.showName().success(
            function (response) {


                $scope.loginName=response.loginName;
            }
        )
    }






    //查询订单信息
    $scope.userOrder= function (page,rows) {

        userService.userOrder($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage).success(
            function (response) {

                $scope.paginationConf.totalItems=response.total;
                $scope.list=response.rows;

            }
        )
    };











    //基本资料
    //1.登录用户个人基本资料展示
    $scope.findUserMessage=function(){
        userService.findUserMessage().success(
            function(response){
                $scope.findUserMessageUser=response;
            }
        )
    };



    $scope.entity={};
    $scope.aa  = function(job){
        $scope.entity.job =job;

    }
    //2修改基本资料
    $scope.updateUserMessage=function(){
        if($scope.f < 10){
            $scope.f = "0" + $scope.f;
        }
        if($scope.d<10){
            $scope.d="0"+$scope.d;
        }
        $scope.entity.birthday = ($scope.c)+ ("-")+ ($scope.f)+ ("-")+($scope.d);

        $scope.entity.locus=($scope.province)+" "+($scope.city)+" "+($scope.town);
        alert($scope.entity.birthday)
        alert($scope.entity.locus)
        $scope.entity.birthday = $scope.entity.birthday + " 00:00:00";
        /*alert(locus)*/
        userService.updateUserMessage( $scope.entity).success(
            function (response) {
                if(response.success){
                    alert("修改完成")
                }else {
                    alert(response.message)
                }

            }
        )
    }


    //展示用户地址
    $scope.findAddress=function () {
        userService.findAddress().success(
            function (response) {//List<TbAddress>
                $scope.addressList=response;

            }
        )
    };


    //新增与修改合并
    $scope.addAddress=function(userAddress){
        var addAndUdate ;
        if(userAddress.id!=null){
            var addAndUdate=userService.updateAddress(userAddress)
        }else {
            var addAndUdate=userService.addAddress(userAddress)
        }
        addAndUdate.success(
            function (response) {
                if (response.success){
                    alert("成功");
                    $scope.findAddress()
                }else {
                    alert(response.message)
                }
            }
        )
    }

    //用户实体查询用于编辑
    $scope.findOneAddress=function (id) {
        userService.findOneAddress(id).success(
            function (response) {
                $scope.userAddress=response;
            }
        )
    };


    //删除用户地址

    $scope.deleteAddress=function (id) {
        userService.deleteAddress(id).success(
            function (response) {
                if(response.success){
                    alert("地址已删除")
                    $scope.findAddress();
                }else {
                    alert(response.message)
                }
            }
        )
    }



});	
