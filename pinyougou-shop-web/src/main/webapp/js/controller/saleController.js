app.controller("saleController",function ($scope,saleService) {
    //定义一个方法 页面一加载 发请求获取销售数据
    $scope.findSellData=function () {

        saleService.findSellData().success(
            function (response) {
                $scope.data=response;
                // 填入数据
                myChart.hideLoading();
                myChart.setOption({
                    xAxis: {
                        data: $scope.data.categories
                    },
                    series: [{
                        // 根据名字对应到相应的系列
                        name: '销量',
                        data: $scope.data.data,

                    }]
                });
            }
        )
    }

});
















