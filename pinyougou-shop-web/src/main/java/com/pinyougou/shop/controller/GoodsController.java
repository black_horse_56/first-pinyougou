package com.pinyougou.shop.controller;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojogroup.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbGoods;
import com.pinyougou.sellergoods.service.GoodsService;

import entity.PageResult;
import entity.Result;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;


/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Resource(name = "generateHtml_topic")
    private Destination generateHtml;

    @Resource(name = "solr_update_index_queue")
    private Destination importItemList;

    @Resource(name = "deleteHtml_topic")
    private Destination deleteHtml;

    @Resource(name = "solr_delete_index_queue")
    private Destination deleteItemList;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<TbGoods> findAll() {
        return goodsService.findAll();
    }


    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return goodsService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param goods
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Goods goods) {
        try {
            //登录的商家的ID
            String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
            goods.getGoods().setSellerId(sellerId);
            goodsService.add(goods);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "增加失败");
        }
    }

    /**
     * 修改
     *
     * @param goods
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Goods goods) {
        try {
            //校验 更新的商品 是不是当前的商家
            String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
            Goods one = goodsService.findOne(goods.getGoods().getId());
            String sellerId1 = one.getGoods().getSellerId();
            if (!sellerId.equals(sellerId1)) {
                return new Result(false, "没有权限修改");
            }

            goodsService.update(goods);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Goods findOne(Long id) {
        return goodsService.findOne(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        try {
            goodsService.delete(ids);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbGoods goods, int page, int rows) {
        //获取当前登录的用户的ID
        goods.setSellerId(SecurityContextHolder.getContext().getAuthentication().getName());

        return goodsService.findPage(goods, page, rows);
    }

    /**更新商品上下架状态
     * @return
     */
    @RequestMapping("/updateIsMarketableStatus")
    public Result updateIsMarketableStatus(Long[] ids,String isMarketable){
        try {
            //调用商品服务完成功能
            goodsService.updateIsMarketableStatus(ids,isMarketable);
            //判断商品上下架状态(isMarketable)是否为1
            if ("1".equals(isMarketable)){
                //商品上架,调用jmsTemplate发送消息 生成静态页面
                jmsTemplate.send(generateHtml, new MessageCreator() {
                    @Override
                    public Message createMessage(Session session) throws JMSException {
                        return session.createObjectMessage(ids);
                    }
                });

                //调用商品服务查询商品id所对应的Sku列表
                List<TbItem> itemList = goodsService.findItemListByGoodsId(ids);
                //商品上架,调用jmsTemplate发送消息 导入上架商品id所对应的SKU数据到索引库中
                jmsTemplate.send(importItemList, new MessageCreator() {
                    @Override
                    public Message createMessage(Session session) throws JMSException {
                        //把itemList转换成json字符串
                        String s = JSON.toJSONString(itemList);
                        return session.createTextMessage(s);
                    }
                });
            }else{
                //商品下架 调用jmsTemplate发送消息 删除静态页面
                jmsTemplate.send(deleteHtml, new MessageCreator() {
                    @Override
                    public Message createMessage(Session session) throws JMSException {
                        return session.createObjectMessage(ids);
                    }
                });

                //商品下架 调用jmsTemplate发送消息 删除SPU id在索引库中所对应的SKU数据
                jmsTemplate.send(deleteItemList, new MessageCreator() {
                    @Override
                    public Message createMessage(Session session) throws JMSException {
                        return session.createObjectMessage(ids);
                    }
                });

            }

            return new Result(true,"更新成功");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false,e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"更新失败");
        }
    }

}
