package com.pinyougou.shop.controller;

import java.util.List;
import java.util.Map;

import com.pinyougou.pojogroup.QueryVo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.sellergoods.service.OrderService;

import entity.PageResult;
import entity.Result;

/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Reference
    private OrderService orderService;


    /**
     * 返回全部列表
     *
     * @return tb_order List
     */
    @RequestMapping("/findAll")
    public List<TbOrder> findAll() {
        return orderService.findAll();
    }


    /**
     * 返回全部列表
     *
     * @return result_list
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {

        // 根据商家的id，获取商家的订单列表，遍历
        String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();

        return orderService.findPage(page, rows);
    }

    /**
     * 增加
     * 并获取商家 sellerId
     *
     * @param order
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody TbOrder order) {

        try {
            orderService.add(order);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "增加失败");
        }
    }

    /**
     * 修改
     *
     * @param order order
     * @return result
     */
    @RequestMapping("/update")
    public Result update(@RequestBody TbOrder order) {
        try {
            orderService.update(order);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public TbOrder findOne(Long id) {
        return orderService.findOne(id);
    }

    /*
     * 批量删除
     * @param ids
     * @return
     */
	/*@RequestMapping("/delete")
	public Result delete(Long [] ids){
		try {
			orderService.delete(ids);
			return new Result(true, "删除成功"); 
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除失败");
		}
	}*/

    /**
     * 查询+分页
     *
     * @param order
     * @return pageResult
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbOrder order, int page, int rows) {

        // 通过商家名，得到商家对应的订单列表
        String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();

        return orderService.findPage(order, page, rows);
    }


    @RequestMapping("/findSellData")
    public Map<String,Object> findSellData(@RequestParam Long timeInterval){
        //获取商家seller_id
		String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
        //调用商品服务完成功能
        //创建一个QueryVo用来存储查询条件
        QueryVo queryVo = new QueryVo();
        queryVo.setSellerId(sellerId);
        queryVo.setTimeInterval(timeInterval);
        return orderService.findSellData(queryVo);
    }

}
